const menu = document.getElementById('menu'),
      mobileMenu = document.querySelector('.mobile__nav'),
      menuBg = document.querySelector('.menu-bg');

menu.addEventListener('click', () => {

    menu.classList.toggle('change');
    mobileMenu.classList.toggle('change');
    menuBg.classList.toggle('change-bg');

    console.log('Is working - changed menu burger')
});